At the top of the Main class, there are three static variables.

The first one is named "base": 
This variable stores the currency name *from* which you want to convert: set to EUR.
In theory you could change it to different currencies and it would work,
but because the API only accepts the request for EUR (free account version limitations), it doesn't.

The second variable is named "accesskey":
In case someone had a different accesskey, you could change that there.

The third variable is named "symbolsArray":
In this array you can store the different *target* currencies you want to convert to.
E.g. if you put "USD", "AUD", the programm creates two different files with the target-curreny-name
as part of file name, makes the conversion in the respective currency and writes it to the file.

Design: 
The project consists of one Main class from which everything is executed.
I also created a directory for the input and output files, to keep order.

Maintainability:
I've tried to make my project as maintainable as possible. 
Everything is flexible; from the base-currency to multiple currencies to different
access_keys.
The only part that is less flexible is the part where I read from the API:
Here I get everything in a string format which I then split into "clean" pieces and remove 
special characters.
I then store the "clean" pieces in a Hashmap with the keys=currency-names and values=currency-values.

Scalability:
Because of the fact, that you can convert into multiple currencies and the program creates different 
files for it, I would say that the scalability is pretty good.

Performance:
The project doesn't need a lot of resources, because no external libraries are used and
the different file writers / file readers are closed/reused.



