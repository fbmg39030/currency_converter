import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class Main {
    private static HttpURLConnection connection;
    static String base = "EUR";
    static String acceskey = "ba00076a565530f42779e3c0f8fd0779";
    //static String symbolsArray[] = {"USD", "AUD", "CHF"};
    static String symbolsArray[] = {"USD"};

    public static void main(String[] args) {
        BufferedReader reader;
        String line;
        StringBuffer responseContent = new StringBuffer();

        try {
            StringBuffer stringURL = new StringBuffer();//creating my url in a string format
            stringURL.append("http://data.fixer.io/api/latest");
            stringURL.append("?access_key=" + acceskey);//adding acceskey
            stringURL.append("&base=" + base);//adding base currency name
            stringURL.append("&symbols=");

            for (int i = 0; i < symbolsArray.length; i++) {
                stringURL.append(symbolsArray[i]);//adding multiple currencies
                if (i + 1 != symbolsArray.length) {
                    stringURL.append(",");//after each currency name the program adds a comma, but not after the last one
                }
            }
            URL url = new URL(stringURL.toString());
            connection = (HttpURLConnection) url.openConnection();//establishing connection

            connection.setRequestMethod("GET"); //we wanna read therefore our method is GET
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(500);

            int status = connection.getResponseCode();
            if (status > 299) {//there was a problem with the connection/api
                reader = new BufferedReader(new InputStreamReader((connection.getErrorStream())));//get error
                while ((line = reader.readLine()) != null) {
                    responseContent.append(line);//print error
                }
                reader.close();
            } else {//everything worked fine
                reader = new BufferedReader(new InputStreamReader((connection.getInputStream())));
                while ((line = reader.readLine()) != null) {
                    responseContent.append(line);//adding api-content to a buffer
                }
                reader.close();

                System.out.println("I got this from api: " + responseContent.toString());

                String apiResponseString = responseContent.toString().replace("{", ""); //removing special character to have each element "clean"
                apiResponseString = apiResponseString.replace("}", "");
                apiResponseString = apiResponseString.replace("\"", "");
                String apiArguments[] = apiResponseString.split(":|,"); //splitting the json content

                HashMap<String, Double> currencyValueMap = new HashMap<String, Double>();
                for (int i = 9; i < apiArguments.length; i += 2) { //starting after "rates" element in json string
                    currencyValueMap.put(apiArguments[i], Double.parseDouble(apiArguments[i + 1])); //i=name of currency i+1=currency value
                }

                File inpf = new File("files/in.txt"); //input file
                inpf.createNewFile(); //creates output file if not exists yet

                for (Map.Entry<String, Double> entry : currencyValueMap.entrySet()) { //for each currency you want to convert to, a file will be created with the converted values
                    String key = entry.getKey(); //e.g USD
                    Double value = entry.getValue();//e.g 1.213703

                    File outf = new File(String.format("files/%sto%s.txt", base, key)); //creating output file e.g.:EURtoUSD
                    outf.createNewFile(); //creates output file if not exists yet
                    FileWriter myWriter = new FileWriter(outf);

                    double convertedValue;

                    Scanner myReader = new Scanner(inpf); //reading input file
                    while (myReader.hasNextLine()) {
                        String data = myReader.nextLine(); //storing every line into a string value
                        convertedValue = value * Double.parseDouble(data); //takes first line of input file and multiplies it by the currency value of the target currency
                        myWriter.write(convertedValue + "\n");
                    }
                    myReader.close();
                    myWriter.close();
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
